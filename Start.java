import java.awt.*;
import java.util.Random;
import java.util.Scanner;

public class Start {
    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        random = new Random();
        System.out.println("STATKI");
        readSettings();
        int minSize = checkMinimalSize();
        int mapSize = width * height;
        if (mapSize < minSize) {
            System.out.println("Plansza jest zbyt mała!");
            System.out.println("Plansza składa się z " + width + "·" + height + "=" + mapSize + " pól.");
            System.out.println("Dla danej ilości statków potrzeba co najmniej " + minSize + " pól.");
            System.exit(0);
        }

        Map aiMap = Generator.generateMap(maxShips, width, height);
        if (aiMap == null) {
            System.err.println("Błąd generowania");
            System.exit(1);
        }

        System.out.println("Wygenerowano planszę dla AI.");
        System.out.println("Uzupełnij swoją planszę");
        Map userMap = Generator.fillByUser(maxShips, width, height);
        if (userMap == null) {
            System.exit(0);
        }

        Game game = new Game(aiMap, userMap);
        game.startGame();
    }

    /*
    Wczytanie rozmiarów planszy i ilości statków
     */
    private static void readSettings() {
        width = readInt("Podaj szerokość planszy: ");
        height = readInt("Podaj wysokość planszy: ");
        maxShips[4] = readInt("Liczba pięciomasztowców: ");
        maxShips[3] = readInt("Liczba czteromasztowców: ");
        maxShips[2] = readInt("Liczba trójmasztowców: ");
        maxShips[1] = readInt("Liczba dwumasztowców: ");
        maxShips[0] = readInt("Liczba jednomasztowców: ");
    }

    /*
    minimalny rozmiar planszy, aby udało się wygenerować planszę
     */
    private static int checkMinimalSize() {
        int minSize = 0;
        //dla statku N-masztowego potrzeba około 2N+4 miejsc.
        for (int i = 0; i < maxShips.length; i++) {
            int size = 2 * (i + 1) + 4;
            minSize += (size * maxShips[i]);
        }
        return minSize;
    }

    public static int readInt(String text) {
        System.out.print(text);
        return Integer.parseInt(scanner.next());
    }

    public static Point readPoint() {
        System.out.print("> ");
        String code = scanner.next().toUpperCase();
        String letter = code.charAt(0) + "";
        int x;
        try {
            x = Integer.parseInt(code.substring(1)) - 1;
        }
        catch (Exception ex) {
            System.out.println("Błędny zapis. Poprawny zapis: litera+liczba, np. \"A1\"");
            return readPoint();
        }
        int y = Map.getIntFromLetter(letter);
        if (y == -1) {
            System.out.println("Błędny zapis. Poprawny zapis: litera+liczba, np. \"A1\"");
            return readPoint();
        }
        return new Point(x, y);
    }

    public static int width;
    public static int height;
    public static int[] maxShips = new int[5];
    public static Scanner scanner;
    public static Random random;
}
