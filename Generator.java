import java.awt.*;
import java.util.*;

public class Generator {


    public static Map generateMap(int[] ships, int width, int height) {
        int iter = 0;
        Map generated = null;
        Map empty = new Map(width, height);
        while (generated == null && iter < 3) {
            generated = generateShip(Arrays.copyOf(ships, ships.length), empty.copy());
            iter++;
        }

        if (generated == null) return null;
        return removeDots(generated);
    }

    /*
    rekurencyjne generowanie mapy
     */
    private static Map generateShip(int[] shipList, Map actualMap) {
        int biggestIndex = -1;
        for (int i = 0; i < shipList.length; i++) {
            if (shipList[i] != 0) biggestIndex = i;
        }

        if (biggestIndex == -1) return actualMap;

        shipList[biggestIndex]--;
        int size = biggestIndex + 1; //rozmiar statku

        //losowanie pozycji
        int y = Start.random.nextInt(actualMap.height);
        int x = Start.random.nextInt(actualMap.width);

        //Jeżeli pozycja jest już zajęta, zwracamy null
        if (actualMap.map[x][y] != ' ') return null;

        ArrayList<Point> points = new ArrayList<>();
        points.add(new Point(x, y));
        actualMap.map[x][y] = 'S';
        size--;
        while (size > 0) {
            //Losujemy kierunek: 0-góra, 1-dół, 2-lewo, 3-prawo
            int orientation = Start.random.nextInt(4);
            int tx = x;
            int ty = y;
            if (orientation == 0) ty--;
            if (orientation == 1) ty++;
            if (orientation == 2) tx--;
            if (orientation == 3) tx++;

            if (tx >= 0 && ty >= 0 &&
                    tx < actualMap.width && ty < actualMap.height &&
                    actualMap.map[tx][ty] == ' ') {
                actualMap.map[tx][ty] = 'S';
                x = tx;
                y = ty;
                points.add(new Point(x, y));
                size--;
            }
        }
        fillWithDots(actualMap, points.toArray(new Point[0]));
        Map nextGeneratedMap = null;
        int iter = 0;
        while (nextGeneratedMap == null && iter < 6) {
            nextGeneratedMap = generateShip(Arrays.copyOf(shipList, shipList.length), actualMap.copy());
            iter++;
        }
        return nextGeneratedMap;
    }

    public static Map fillByUser(int[] shipList, int width, int height) {
        Map map = new Map(width, height);
        for (int i = shipList.length - 1; i >= 0; i--) {
            int size = i + 1;
            while (shipList[i] != 0) {
                map = fillShip(size, map);
                shipList[i]--;
            }
        }
        return removeDots(map);
    }

    private static Map fillShip(int shipSize, Map map) {
        boolean done = false;
        while (!done) {
            done = true;
            Map work = map.copy();
            Point[] points = new Point[shipSize];
            int currentSize = shipSize;
            while (currentSize > 0 && done) {
                currentSize--;
                System.out.println(work);

                switch(shipSize) {
                    case 1:
                        System.out.println("Ustaw jednomasztowiec");
                        break;
                    case 2:
                        System.out.println("Ustaw dwumasztowec");
                        break;
                    case 3:
                        System.out.println("Ustaw trójmasztowiec");
                        break;
                    case 4:
                        System.out.println("Ustaw czteromasztowiec");
                        break;
                    case 5:
                        System.out.println("Ustaw pięciomasztowiec");
                }
                System.out.println("Wprowadź pole, np. A1");
                Point read = Start.readPoint();
                int x = read.x;
                int y = read.y;
                points[currentSize] = read;

                if (!work.setWithCheck('S', x, y)) {
                    done = false;
                    if (work.map[x][y] == 'S') {
                        System.out.println("Statek jest już umieszczony w tym polu");
                    }
                    else if (work.map[x][y] == '·') {
                        System.out.println("Między statkami musi znajdować się co najmniej 1 pole przerwy");
                    }
                }
            }
            if (!done) continue;
            if (points.length != 1) {
                Queue<Point> toConsume = new LinkedList<>();
                toConsume.addAll(Arrays.asList(points));
                Queue<Point> queue = new LinkedList<>();
                queue.add(toConsume.remove());
                while (!queue.isEmpty() && !toConsume.isEmpty()) {
                    Point tile = queue.remove();
                    findNeighbours(tile, toConsume)
                            .forEach(n -> {
                                toConsume.remove(n);
                                queue.add(n);
                            });
                }
                if (!toConsume.isEmpty()) {
                    System.out.println("Brak połączenia między masztami statku");
                    done = false;
                }
            }
            if (done) {
                fillWithDots(work, points);
                return work;
            }
        }
        return map;
    }

    private static ArrayList<Point> findNeighbours(Point tile, Collection<Point> tiles) {
        ArrayList<Point> ret = new ArrayList<>();
        for (Point p : tiles){
            if (distance(tile, p) == 1) ret.add(p);
        }
        return ret;
    }

    private static int distance(Point a, Point b) {
        int dx = a.x - b.x;
        int dy = a.y - b.y;
        return Math.abs(dx) + Math.abs(dy);
    }

    private static void fillWithDots(Map map, Point[] points) {
        for (Point p : points) {
            map.setWithCheck('·', p.x + 1, p.y);
            map.setWithCheck('·', p.x - 1, p.y);
            map.setWithCheck('·', p.x, p.y + 1);
            map.setWithCheck('·', p.x, p.y - 1);
            map.setWithCheck('·', p.x + 1, p.y + 1);
            map.setWithCheck('·', p.x + 1, p.y - 1);
            map.setWithCheck('·', p.x - 1, p.y + 1);
            map.setWithCheck('·', p.x - 1, p.y - 1);
        }
    }

    private static Map removeDots(Map generated) {
        for (int i = 0; i < generated.width; i++) {
            for (int j = 0; j < generated.height; j++) {
                if (generated.map[i][j] == '·') {
                    generated.map[i][j] = ' ';
                }
            }
        }
        return generated;
    }
}
