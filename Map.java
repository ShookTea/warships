import java.awt.*;
import java.util.*;

public class Map {
    public Map(int width, int height) {
        this.width = width;
        this.height = height;
        map = new char[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                map[i][j] = ' ';
            }
        }
    }

    public Map copy() {
        Map m = new Map(width, height);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                m.map[i][j] = this.map[i][j];
            }
        }
        return m;
    }

    public boolean setWithCheck(char c, int x, int y) {
        if (x >= 0 && y >= 0 && x < width && y < height && map[x][y] == ' ') {
            map[x][y] = c;
            return true;
        }
        return false;
    }

    public boolean shipExists() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (map[x][y] == 'S') return true;
            }
        }
        return false;
    }

    public Point getRandomShot() {
        ArrayList<Point> points = new ArrayList<>();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (map[x][y] == 'S' || map[x][y] == ' ') {
                    points.add(new Point(x, y));
                }
            }
        }
        if (points.size() == 0) return null;
        return points.get(Start.random.nextInt(points.size()));
    }

    /*
    0 - pudło, 1 - trafiony, 2 - trafiony zatopiony, -1 - błędny strzał, -2 - strzał poza zakresem
     */
    public int shot(Point p) {
        if (p.x < 0 || p.y < 0 || p.x >= width || p.y >= height) return -2;
        if (map[p.x][p.y] == 'S') {
            map[p.x][p.y] = 'X';
            int ret = checkSink(p.x, p.y) ? 2 : 1;
            if (ret == 2) {
                surroundWithDots(p);
            }
            return ret;
        }
        else if (map[p.x][p.y] == ' ') {
            map[p.x][p.y] = '·';
            return 0;
        }
        return -1;
    }

    private boolean checkSink(int x, int y) {
        ArrayList<Point> points = new ArrayList<>();
        Queue<Point> queue = new LinkedList<>();
        queue.add(new Point(x, y));
        while (!queue.isEmpty()) {
            Point p = queue.remove();
            points.add(p);
            for (Point n : getSurround(p)) {
                if (map[n.x][n.y] == 'S') return false;
                if (!points.contains(n) && map[n.x][n.y] == 'X') {
                    queue.add(n);
                }
            }
        }
        return true;
    }

    public void surroundWithDots(Point shot) {
        ArrayList<Point> points = new ArrayList<>();
        Queue<Point> queue = new LinkedList<>();
        queue.add(shot);
        while (!queue.isEmpty()) {
            Point p = queue.remove();
            points.add(p);
            for (Point n : getSurround(p)) {
                if (map[n.x][n.y] == ' ') {
                    map[n.x][n.y] = '·';
                }
                if (!points.contains(n) && (map[n.x][n.y] == 'X' || map[n.x][n.y] == ' ')) {
                    queue.add(n);
                }
            }
        }
    }

    private Point[] getSurround(Point p) {
        ArrayList<Point> arr = new ArrayList<>();
        for (int x = p.x - 1; x <= p.x + 1; x++) {
            for (int y = p.y - 1; y <= p.y + 1; y++) {
                if (x >= 0 && x < width && y >= 0 && y < height && !(x == p.x && y == p.y)) {
                    arr.add(new Point(x, y));
                }
            }
        }
        return arr.toArray(new Point[0]);
    }

    @Override
    public String toString() {
        //pozycje liczbowe
        String ret = "   ";
        for (int x = 0; x < width; x++) {
            ret += "  " + (x+1);
            if (x < 9) {
                ret += " ";
            }
        }
        ret += " \n";
        //górna linia ramki
        ret += "   ┌";
        for (int x = 0; x < width - 1; x++) {
            ret += "───┬";
        }
        ret += "───┐\n";

        //zawartość
        for (int y = 0; y < height; y++) {
            ret += " " + getLetterFromInt(y) + " │";
            for (int x = 0; x < width; x++) {
                ret += " " + map[x][y] + " │";
            }
            ret += "\n";

            //środkowa linia ramki
            if (y < height - 1) {
                ret += "   ├";
                for (int x = 0; x < width - 1; x++) {
                    ret += "───┼";
                }
                ret += "───┤\n";
            }
        }
        //dolna linia ramki
        ret += "   └";
        for (int x = 0; x < width - 1; x++) {
            ret += "───┴";
        }
        ret += "───┘";
        return ret;
    }

    public static String getLetterFromInt(int i) {
        return letters[i];
    }

    public static int getIntFromLetter(String letter) {
        for (int i = 0; i < letters.length; i++) {
            if (letters[i].equals(letter)) return i;
        }
        return -1;
    }

    //' ' - puste, 'S' - maszt, '·' - pudło, 'X' - maszt zestrzelony
    public char[][] map;
    public final int width;
    public final int height;

    private static String[] letters = new String[] {
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
            "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
    };
}
