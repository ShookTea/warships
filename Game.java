import java.awt.*;

public class Game {
    public Game(Map ai, Map player) {
        this.ai = ai;
        this.player = player;
        this.fogOfWar = new Map(ai.width, ai.height);
        playerTurn = Start.random.nextBoolean();
    }

    public void startGame() {
        while(checkGameStatus() == 0) {
            if (playerTurn) {
                playerMove();
            }
            else {
                aiMove();
            }
            playerTurn = !playerTurn;
        }

        if (checkGameStatus() == 1) {
            System.out.println("---=== GRA SKOŃCZONA - WYGRANA ===---");
        }
        else {
            System.out.println("---=== GRA SKOŃCZONA - PRZEGRANA ===---");
        }
        printGameStatus(false);
    }

    private void playerMove() {
        printGameStatus(true);
        if (!lastPlayerMove.isEmpty()) System.out.println(lastPlayerMove);
        if (!lastAiMove.isEmpty()) System.out.println(lastAiMove);
        System.out.println("Ruch gracza:");
        Point shot = Start.readPoint();
        int shotResult = ai.shot(shot);
        while(shotResult < 0) {
            if (shotResult == -1) System.out.println("Nie możesz strzelić w pole, w które już strzelałeś.");
            if (shotResult == -2) System.out.println("Wykroczyłeś poza zakres tablicy.");
            shot = Start.readPoint();
            shotResult = ai.shot(shot);
        }
        switch (shotResult) {
            case 0:
                lastPlayerMove = "PUDŁO";
                fogOfWar.map[shot.x][shot.y] = '·';
                break;
            case 1:
                lastPlayerMove = "TRAFIONY";
                fogOfWar.map[shot.x][shot.y] = 'X';
                break;
            case 2:
                lastPlayerMove = "TRAFIONY - ZATOPOPIONY!";
                fogOfWar.map[shot.x][shot.y] = 'X';
                fogOfWar.surroundWithDots(shot);
                break;
        }
    }

    private void aiMove() {
        lastAiMove = "Ruch AI: ";
        Point shot = player.getRandomShot();
        String row = Map.getLetterFromInt(shot.y);
        int column = shot.x + 1;
        lastAiMove += row + column + " - ";

        switch (player.shot(shot)) {
            case 0:
                lastAiMove += "PUDŁO";
                break;
            case 1:
                lastAiMove += "TRAFIONY";
                break;
            case 2:
                lastAiMove += "TRAFIONY - ZATOPOPIONY!";
                break;
        }
    }

    /*
    0 - gra wciąż trwa, 1 - gracz wygrał, -1 - AI wygrało
     */
    private int checkGameStatus() {
        if (!ai.shipExists()) return 1;
        if (!player.shipExists()) return -1;
        return 0;
    }

    private void printGameStatus(boolean showFog) {
        System.out.println();
        printHeader();
        System.out.println();
        Map mapToDisplay = showFog ? fogOfWar : ai;
        String[] first = mapToDisplay.toString().split("\n");
        String[] second = player.toString().split("\n");
        for (int i = 0; i < first.length; i++) {
            System.out.println(first[i] + "   " + second[i]);
        }
    }

    private void printHeader() {
        int onScreenWidth = (ai.width + 1) * 2;
        for (int i = 0; i < onScreenWidth; i++) {
            System.out.print(" ");
        }
        System.out.print("Plansza AI");
        for (int i = 0; i < onScreenWidth * 2 - 12; i++) {
            System.out.print(" ");
        }
        System.out.println("Plansza gracza");
    }

    private final Map ai;
    private final Map fogOfWar;
    private final Map player;
    private boolean playerTurn;
    private String lastAiMove = "";
    private String lastPlayerMove = "";
}
